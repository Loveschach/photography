﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	enum PlayerState {
		idle,
		walk,
		airborne,
	};

	// Player Speed Constants
	float PLAYER_SPEED = 6.5f;
	float CROUCH_PLAYER_SPEED = 3.25f;
	float ZOOM_PLAYER_SPEED = 2f;
	float LOOK_SPEED = 1f;

	// Camera Constants
	float MAX_CAMERA_Y = 70;
	float MIN_CAMERA_Y = -70;
	float PHOTO_COOLDOWN = 1.25f;

	// Jump Constants
	float JUMP_FORCE = 250f;
	float GROUNDED_EPSILON = 0.001f;

	// Input Saving
	bool m_hasJumped = false;

	PlayerState m_currentState;
	float m_lastPhotoTime;
	Rigidbody m_body;
	Camera m_camera;
	ViewModel m_viewModel;
	Vector2 m_currentMouseLook;
	CapsuleCollider m_collider;
	PlayerCamera m_playerCamera;

    // Start is called before the first frame update
    void Start()
    {
		m_currentState = PlayerState.idle;
		m_body = GetComponent<Rigidbody>();
		m_collider = GetComponent<CapsuleCollider>();
		m_camera = GetComponentInChildren<Camera>();
		m_viewModel = GetComponentInChildren<ViewModel>();
		m_currentMouseLook = new Vector2( 0, 0 );
		m_playerCamera = GetComponent<PlayerCamera>();
		m_lastPhotoTime = -PHOTO_COOLDOWN;
		Cursor.lockState = CursorLockMode.Locked;
    }

	void EnterIdle() {
		m_viewModel.StartIdle();
	}

	void EnterWalk() {
		m_viewModel.StartWalk();
	}

	void EnterAirborne() {
		m_viewModel.StartJump();
	}

	void EnterState( PlayerState state ) {
		if ( state == m_currentState ) {
			return;
		}

		ExitState( m_currentState );
		switch( state ) {
			case PlayerState.idle:
				EnterIdle();
				break;
			case PlayerState.walk:
				EnterWalk();
				break;
			case PlayerState.airborne:
				EnterAirborne();
				break;
			default:
				break;
		}

		m_currentState = state;
	}

	void ExitIdle() {
	}

	void ExitState( PlayerState state ) {
		switch ( state ) {
			case PlayerState.idle:
				ExitIdle();
				break;
			default:
				break;
		}
	}

	float GetPlayerSpeed() {
		if( m_playerCamera.GetZoom() ) {
			return ZOOM_PLAYER_SPEED;
		} else if ( m_playerCamera.GetCrouch() ) {
			return CROUCH_PLAYER_SPEED;
		} else {
			return PLAYER_SPEED;
		}
	}

	void UpdateMouseLook() {
		Vector2 mouseLook = new Vector2( Input.GetAxisRaw( "Mouse X" ), Input.GetAxisRaw( "Mouse Y" ) );
		mouseLook *= LOOK_SPEED;

		m_currentMouseLook.x += mouseLook.x;
		m_currentMouseLook.y -= mouseLook.y;
		m_currentMouseLook.y = Mathf.Clamp( m_currentMouseLook.y, MIN_CAMERA_Y, MAX_CAMERA_Y );
		m_camera.transform.localEulerAngles = new Vector3( m_currentMouseLook.y, 0, 0 );
		transform.localEulerAngles = new Vector3( 0, m_currentMouseLook.x, 0 );
	}

	void UpdateMove() {
		float verticalInput = Input.GetAxisRaw( "Vertical" );
		float horizontalInput = Input.GetAxisRaw( "Horizontal" );
		Vector3 forward = transform.forward * verticalInput;
		Vector3 right = transform.right * horizontalInput;
		Vector3 move = Vector3.Normalize( forward + right );
		Vector3 adjustedMove = move * GetPlayerSpeed() * Time.deltaTime;
		m_body.MovePosition( transform.position + adjustedMove );
	}

	void SetupComponentsForPhoto( bool enabled ) {
		m_viewModel.SetRendering( !enabled );
	}

	void DoCamera() {
		if ( Input.GetAxisRaw( "Photo" ) != 0 && ( Time.time - m_lastPhotoTime ) > PHOTO_COOLDOWN ) {
			SetupComponentsForPhoto( true );
			m_playerCamera.CapturePhoto();
			SetupComponentsForPhoto( false );
			m_lastPhotoTime = Time.time;
		}

		if ( Input.GetAxisRaw( "Zoom" ) != 0 ) {
			m_playerCamera.SetZoom( true );
		} else {
			m_playerCamera.SetZoom( false );
		}
	}

	void DoCrouch() {
		if ( Input.GetAxisRaw( "Crouch" ) != 0 ) {
			m_playerCamera.SetCrouch( true );
		}
		else {
			m_playerCamera.SetCrouch( false );
		}
	}

	void DoJump() {
		if ( m_hasJumped ) {
			m_body.AddForce( Vector3.up * JUMP_FORCE );
			EnterState( PlayerState.airborne );
			m_hasJumped = false;
		}
	}

	void CheckJump() {
		if ( Input.GetAxisRaw( "Jump" ) != 0 ) {
			m_hasJumped = true;
		}
	}

	void UpdateGroundedState() {
		float verticalInput = Input.GetAxisRaw( "Vertical" );
		float horizontalInput = Input.GetAxisRaw( "Horizontal" );
		// TODO: If you switch back the animation changes.
		if ( verticalInput == 0 && horizontalInput == 0 ) {
			EnterState( PlayerState.idle );
		}
		else {
			EnterState( PlayerState.walk );
		}
	}

	void UpdateGrounded( bool isFixed ) {
		if( isFixed ) {
			UpdateMove();
			DoJump();
		} else {
			CheckJump();
			UpdateMouseLook();
			if ( !IsGrounded() ) {
				EnterState( PlayerState.airborne );
			}

			UpdateGroundedState();
			DoCamera();
			DoCrouch();
		}
	}

	bool IsGrounded() {
		float yVelocity = m_body.velocity.y;
		Vector3 overlapPoint = new Vector3( m_collider.bounds.center.x, m_collider.bounds.center.y - m_collider.bounds.extents.y, m_collider.bounds.center.z );
		LayerMask mask = LayerMask.GetMask( "Ground" );

		Collider[] overlap = Physics.OverlapSphere( overlapPoint, 0.5f, mask );
		return overlap.Length > 0;
	}

	void UpdateAirborne( bool isFixed ) {
		if( isFixed ) {
			UpdateMove();
		} else {
			UpdateMouseLook();
			if ( IsGrounded() ) {
				UpdateGroundedState();
			}

			DoCamera();
		}
	}

	void UpdateState( bool isFixed ) {
		switch ( m_currentState ) {
			case PlayerState.idle:
			case PlayerState.walk:
				UpdateGrounded( isFixed );
				break;
			case PlayerState.airborne:
				UpdateAirborne( isFixed );
				break;
			default:
				break;
		}
	}

    // Update is called once per frame
    void Update()
    {
		UpdateState( false );
    }

	// Update is called once per frame
	void FixedUpdate() {
		UpdateState( true );
	}
}
