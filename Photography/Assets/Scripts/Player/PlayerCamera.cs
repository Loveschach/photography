﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCamera : MonoBehaviour
{
	// Photo Constants
	int MAX_PHOTOS = 10;
	int PHOTO_WIDTH = 800;
	int PHOTO_HEIGHT = 600;

	// Zoom Constants
	int DEFAULT_FOV = 60;
	int MAX_ZOOM_FOV = 30;
	float ZOOM_TIME = 0.5f;

	// Crouch Constants
	float LOWEST_CROUCH_HEIGHT = -0.375f;
	float CROUCH_TIME = 0.5f;

	List<Photo> m_photos;
	Camera m_camera;
	RawImage m_rawImage;
	AudioSource m_shutterSource;
	Animator m_shutterAnimator;

	// Zoom Variables 
	float m_currentFOV;
	float m_zoomStartFOV;
	float m_zoomStartTime;
	bool m_zooming;

	// Crouch Variables
	float m_defaultCameraHeight;
	float m_currentCrouchHeight;
	float m_crouchStartHeight;
	float m_crouchStartTime;
	bool m_crouching;

	// Start is called before the first frame update
	void Start()
    {
		m_photos = new List<Photo>( MAX_PHOTOS );
		m_camera = GetComponentInChildren<Camera>();
		GameObject UIPhoto = GameObject.Find( "RenderPhoto" );
		m_rawImage = UIPhoto.GetComponent<RawImage>();
		GameObject UIShutter = GameObject.Find( "Shutter" );
		m_shutterAnimator = UIShutter.GetComponent<Animator>();
		m_shutterSource = GetComponent<AudioSource>();
		m_currentFOV = DEFAULT_FOV;
		m_defaultCameraHeight = m_camera.transform.localPosition.y;
		m_currentCrouchHeight = m_defaultCameraHeight;
	}

    public void CapturePhoto() {
		RenderTexture texture = new RenderTexture( PHOTO_WIDTH, PHOTO_HEIGHT, 24 );
		RenderTexture currentCameraTexture = m_camera.targetTexture;
		m_camera.targetTexture = texture;
		m_camera.Render();

		RenderTexture currentActiveTexture = RenderTexture.active;
		RenderTexture.active = texture;

		Texture2D photo = new Texture2D( PHOTO_WIDTH, PHOTO_HEIGHT, TextureFormat.RGB24, false );
		photo.ReadPixels( new Rect( 0, 0, PHOTO_WIDTH, PHOTO_HEIGHT ), 0, 0 );
		photo.Apply();

		m_camera.targetTexture = currentCameraTexture;
		RenderTexture.active = currentActiveTexture;
		Destroy( texture );

		m_photos.Add( new Photo( photo ) );
		m_rawImage.texture = photo;
		m_shutterSource.Play();
		m_shutterAnimator.SetTrigger( "Shutter" );
	}

	private void FixedUpdate() {
		if ( ( m_zooming && m_currentFOV != MAX_ZOOM_FOV ) || ( !m_zooming && m_currentFOV != DEFAULT_FOV ) ) {
			int endFOV = m_zooming ? MAX_ZOOM_FOV : DEFAULT_FOV;
			float zoomTime = Mathf.Abs( ( ( m_zoomStartFOV - endFOV ) / ( DEFAULT_FOV - MAX_ZOOM_FOV ) ) * ZOOM_TIME );

			m_currentFOV = Mathf.SmoothStep( m_zoomStartFOV, endFOV, ( Time.time - m_zoomStartTime ) / zoomTime );
			m_camera.fieldOfView = m_currentFOV;
		}

		if ( ( m_crouching && m_currentCrouchHeight != LOWEST_CROUCH_HEIGHT ) || ( !m_crouching && m_currentCrouchHeight != m_defaultCameraHeight ) ) {
			float endHeight = m_crouching ? LOWEST_CROUCH_HEIGHT : m_defaultCameraHeight;
			float crouchTime = Mathf.Abs( ( ( m_crouchStartHeight - endHeight ) / ( m_defaultCameraHeight - LOWEST_CROUCH_HEIGHT ) ) * CROUCH_TIME );

			m_currentCrouchHeight = Mathf.SmoothStep( m_crouchStartHeight, endHeight, ( Time.time - m_crouchStartTime ) / crouchTime );
			m_camera.transform.localPosition = new Vector3( m_camera.transform.localPosition.x, m_currentCrouchHeight, m_camera.transform.localPosition.z );
		}
	}

	public void SetZoom( bool zoom ) {
		if ( m_zooming != zoom ) {
			m_zoomStartFOV = m_currentFOV;
			m_zoomStartTime = Time.time;
			m_zooming = zoom;
		}
	}

	public bool GetZoom() {
		return m_zooming;
	}

	public void SetCrouch( bool crouch ) {
		if ( m_crouching != crouch ) {
			m_crouchStartHeight = m_currentCrouchHeight;
			m_crouchStartTime = Time.time;
			m_crouching = crouch;
		}
	}

	public bool GetCrouch() {
		return m_crouching;
	}
}
