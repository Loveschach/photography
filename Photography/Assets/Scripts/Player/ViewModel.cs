﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewModel : MonoBehaviour
{
	Animator m_animator;
	MeshRenderer m_renderer;
    // Start is called before the first frame update
    void Start()
    {
		m_animator = GetComponent<Animator>();
		m_renderer = GetComponent<MeshRenderer>();
    }

    public void StartWalk() {
		m_animator.ResetTrigger( "StandTrigger" );
		m_animator.SetTrigger( "WalkTrigger" );
	}

	public void StartIdle() {
		m_animator.ResetTrigger( "WalkTrigger" );
		m_animator.SetTrigger( "StandTrigger" );
	}

	public void StartJump() {
		m_animator.ResetTrigger( "WalkTrigger" );
		m_animator.SetTrigger( "StandTrigger" );
	}

	public void SetRendering( bool rendering ) {
		m_renderer.enabled = rendering;
	}
}
