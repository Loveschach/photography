﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photo
{
	public Texture2D Texture { get; }
	public Photo( Texture2D texture ) {
		Texture = texture;
	}
}
