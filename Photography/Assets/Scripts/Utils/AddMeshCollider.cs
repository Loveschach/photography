﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddMeshCollider : MonoBehaviour
{
	void Reset() {
		Mesh mesh = null;
		MeshFilter meshFilter = GetComponent<MeshFilter>();
		if ( meshFilter != null ) {
			mesh = meshFilter.sharedMesh;
		}

		SkinnedMeshRenderer SkinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
		if ( SkinnedMeshRenderer != null ) {
			mesh = SkinnedMeshRenderer.sharedMesh;
		}
			
		if ( mesh != null ) {
			MeshCollider collider = GetComponent<MeshCollider>();
			if ( collider == null ) {
				collider = gameObject.AddComponent<MeshCollider>();
			}
			collider.sharedMesh = mesh;
		}
	}
}
